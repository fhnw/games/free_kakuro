/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;

import ch.kri.freekakuro.data_classes.Cell;
import ch.kri.freekakuro.data_classes.CellCell;
import ch.kri.freekakuro.data_classes.Game;
import ch.kri.freekakuro.data_classes.CellSum;
import ch.kri.freekakuro.data_classes.PossibleValues;

// There are several sample games hard-coded at the bottom of this class.
// These can simply be returned: return sampleGame1(mainProgram);
//
// Developing our own games is done as follows: 
//
// 1. Create a game layout. Conceptually, this is the arrangement of
//    white and gray squares. Practically speaking, this is the set
//    of CellSum objects (just like in a sample game), however, with
//    no particular sum set. E.g., we know that there is a 3-cell
//    horizontal sum at position (x,y), but we do not yet know its
//    value.
//
// 2. Sort the sums into a list by increasing length. This serves to
//    reduce the effort required to generate a valid game.
//
// 3. In an iterative process:
//    a. Select the next sum with no sum-value
//    b. Create the set of all possible sum-values for the sum
//      b1. Create the set of all possible Possible-Values (PVs) for
//          the given length
//      b2. For each PV
//          - For each cell
//          - - If the intersection of PV and cell-PV is empty
//          - - - Remove PV from the list of possible PVs
//    c. Randomly select one of the remaining possible PVs, and set the
//       value of the sum accordingly
//
// 4. At the end of the recursion (no remaining undefined sums),
//    solve the resulting puzzle.
//
// Step (3b) may fail at any point, i.e., at the end of the process there
// may be no valid possibilities remaining. Step (3e) may fail if we have
// created a puzzle that is inconsistent, that has multiple solutions, or
// that is simply too difficult for our solver.
//
// Note about efficiency: random trial-and-error is not especially fast.
// Our only constraint to prevent wasted time is step b2. Improving the
// game generation process would be good!
public class GameCreator {
	private Random rand; // random number generator for creating games
	private FreeKakuro mainProgram;

	public GameCreator(FreeKakuro mainProgram) {
		this.mainProgram = mainProgram;

		// Initialize random seed (or fetch from given game-number)
		rand = new Random();
		int seed = mainProgram.gameNumber;
		if (seed < 0) { // create a seed from 0 to 99999 and save
			seed = rand.nextInt(100000);
			mainProgram.gameNumber = seed;
		}
		rand.setSeed(seed);
	}
	
	public  Game createGame() {
		// if (true) return sampleGame3();
		
		// Note that a particular attempt may not lead to any valid games.
		// Hence, we try, try, try again until we get a valid game...
		
		Game game = null;
		boolean validGame = false;
		while (!validGame) {
			// Generate layout
			game = createLayout(mainProgram);
		
			// Sort the list of sums by length - this is irrelevant to the game,
			// but important to the efficiency of game generation
			//game.sortSums(); TODO
		
			// Start off the recursive process
			validGame = instantiateSums(game);
			
			if (validGame) {
				// verify game by solving it. While generating the game, we have restricted
				// the possible values of the cells. To solve the game, we must re-create
				// it using only the information on the sums.
				
				Game game2 = new Game(mainProgram.gameWidth, mainProgram.gameHeight);
				for (CellSum sum : game.sums) {
					createSum(game2, sum.getDirection(), sum.getX(),sum.getY(),sum.getSize(),sum.getSum());
				}
				game = game2;
				
				GameSolver solver = new GameSolver(game2);
				boolean progress = true;
				System.out.print("Solving: ");
				while (progress) {
					progress = solver.stepSolver();
				}
				System.out.println("");
				// Are all cells ok? If so, we have a solution - and hence a game!
				for (CellCell cell : game.cells) {
					if (!cell.isUnique()) {
						validGame = false;
						break;
					}
				}
			}
		}
		return game;
	}

	/**
	 * generateLayout should eventually generate a layout at random. A layout
	 * is simply a game in which the sum-value of the sums has not been set.
	 * For the moment, however, this method returns a fixed layout.
	 */ 
	private  Game createLayout(FreeKakuro mainProgram) {
		// TODO should generate patterns rather than using a single fixed one
		mainProgram.gameWidth = 9;
		mainProgram.gameHeight = 10;
		Game game = new Game(mainProgram.gameWidth, mainProgram.gameHeight);

		createEmptySum(game,CellSum.Direction.VERTICAL, 1,1,2);
		createEmptySum(game,CellSum.Direction.HORIZONTAL, 0, 3, 2);
		createEmptySum(game,CellSum.Direction.HORIZONTAL, 0, 2, 3);
		createEmptySum(game,CellSum.Direction.VERTICAL, 2,0,3);
		createEmptySum(game,CellSum.Direction.VERTICAL, 3,0,2);
		createEmptySum(game,CellSum.Direction.HORIZONTAL, 1, 1, 2);
		

		// createEmptySum(game, CellSum.Direction.VERTICAL, 1, 0, 3);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 2, 0, 4);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 4, 0, 5);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 5, 0, 3);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 7, 0, 5);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 8, 0, 4);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 3, 2, 4);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 6, 3, 4);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 5, 4, 5);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 1, 5, 4);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 2, 5, 4);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 4, 6, 3);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 7, 6, 3);
		// createEmptySum(game, CellSum.Direction.VERTICAL, 8, 6, 3);
		//
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 0, 1, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 0, 2, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 0, 3, 5);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 0, 6, 3);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 0, 7, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 0, 8, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 0, 9, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 1, 4, 3);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 2, 5, 5);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 3, 1, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 3, 2, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 3, 7, 5);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 3, 8, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 3, 9, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 4, 6, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 5, 4, 3);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 6, 1, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 6, 2, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 6, 3, 2);
		// createEmptySum(game, CellSum.Direction.HORIZONTAL, 6, 8, 2);
		//		createEmptySum(game, CellSum.Direction.HORIZONTAL, 6, 9, 2);

		return game;
	}

	Notes: need to enable full-scale backtracking, otherwise this will be horribly
	inefficient. Something like:
		
	InstantiateCellSum(currentStatus) {
		if (remainingSum == 0) {
		  return currentStatus
		} else {
			Pick first sum
			create list of PossibleValues
			while (remaining possible values) {
				for each possible instantiation {
					NewStatus = clone(currentStatus)
					instantiate sum
					recurse: InstantiateCellSum(newStatus)
					If result not null, return result
				}
			}
		}		  	
	}
	
	
	// Sets the values for all sums in the game - see class documentation for
	// further information.
	private  boolean instantiateSums(Game game) {
		boolean solutionFound = true;
		boolean abort = false; // set if we discover we have wandered down a blind alley
		
		for (CellSum thisSum : game.sums) {
			// All possible sum-values
			ArrayList<Integer> possibleSumValues = getSumValues(thisSum.getSize());

			ArrayList<PossibleValues> allPossibleValues = new ArrayList<PossibleValues>();
			// All PossibleValues over all sum-values
			for (int i = 0; i < possibleSumValues.size(); i++) {
				int sumValue = possibleSumValues.get(i);
				int patternKey = PossibleValues.getPatternKey(thisSum.getSize(), sumValue);
				allPossibleValues.addAll(game.patterns.get(patternKey));
			}

			// Randomize order of PV list. TODO
			
			// Eliminate possible values that conflict with existing cell constraints.
			// This involves two checks:
			// 1. Collect a list of all values that are already committed in cells.
			//    All of these values must be present in a PV.
			// 2. For cells with lesser restrictions (2 to 8 possible values), check
			//    that some permutation of the PV fits in the cells.
			PossibleValues committedValues = new PossibleValues();
			ArrayList<PossibleValues> cellPVs = new ArrayList<PossibleValues>();
			for (CellCell cell : thisSum.getCells()) {
				PossibleValues cellPV = cell.getPossibleValues();
				if (cell.isUnique()) {
					committedValues.add(cell.getUniqueValue());
				} else if (cellPV.size() < 9) {
					cellPVs.add(cellPV);
				}
			}

			if (committedValues.size() > 0) {
				for (Iterator<PossibleValues> i = allPossibleValues.iterator(); i.hasNext(); ) {
					PossibleValues pv = i.next();
					if (!pv.containsAll(committedValues)) {
						i.remove();
					} else {
						Remove committed values and ensure that remaining values fit in cellPVs
					}
				}
			}
			
			// If all PVs have been eliminated, we are dead in the water
			if (allPossibleValues.size() == 0) {
				solutionFound = false;
				break;
			}

			// Randomly select a set of PossibleValues, and place it in the sum.
			// First we set the sum - setSum causes numerous changes to propogate,
			// as possible values are restricted for various cells. Then we randomly
			// place values in each cell, where these are not already set.
			int i = rand.nextInt(allPossibleValues.size());
			PossibleValues selectedPV = new PossibleValues(allPossibleValues.get(i));
			int selectedSum = selectedPV.sum();
			thisSum.setSum(selectedSum);
			
			// Remove restricted values (i.e., cells that already have values)
			for (Integer val : committedValues) {
				selectedPV.remove(val);
			}
			
			// Now place remaining values. 
			
			Never place a value into a sum where that value is already present. If this cannot be avoided, abort
			the game generation and try again.
			
			for (CellCell cell : thisSum.getCells()) {
				if (!cell.isUnique()) {
					int which = rand.nextInt(selectedPV.size());
					int selectedValue = 0;
					int j = 0;
					for (Iterator<Integer> it = selectedPV.iterator() ; it.hasNext(); ) {
						selectedValue = it.next();
						if (j==which) {
							it.remove();
							break; 
						}
						j++;
					}
					cell.setPossibleValues(new PossibleValues(selectedValue));

					TODO: remove this value from the possible values of all unset cells
					in the cross-sum
					
					System.out.println("Value " + selectedValue + " placed at " + cell.getX() + ", " + cell.getY());
				}
			}
			
			// Check that the assignment has not resulted in any conflicts.
			// This should never happen - but we are being paranoid...
			for (CellCell cell : thisSum.getCells()) {
				if (cell.isImpossible()) {
					solutionFound = false;
					break;
				}
			}			
		}
		return solutionFound;
	}

	//---------------------------------------------------------------
	// Utility functions
	//---------------------------------------------------------------

	/**
	 * Create a list of all possible sum-values for a sum of the given
	 * length. For example, a sum of length 2 can have any sum-value
	 * from 3 through 17. This list is ordered randomly.
	 */
	private  ArrayList<Integer> getSumValues(int length) {
		int minSum = length * (length + 1) / 2;
		int maxSum = minSum + length * (9 - length);
		ArrayList<Integer> sums = new ArrayList<Integer>();
		for (int i = minSum; i <= maxSum; i++) {
			sums.add(i);
		}
		return sums;
	}
	
	private  void createEmptySum(Game game, CellSum.Direction dir, int x, int y, int length) {
		CellSum newSum = new CellSum(dir, x, y, length);
		game.gameGrid[x][y] = newSum;
		game.sums.add(newSum);
		for (int i = 1; i <= length; i++) {
			CellCell newCell;
			if (dir == CellSum.Direction.VERTICAL) {
				if (game.gameGrid[x][y + i] == null) {
					newCell = new CellCell(x, y + i);
					game.cells.add(newCell);
					game.gameGrid[x][y + i] = newCell;
				} else {
					newCell = (CellCell) game.gameGrid[x][y + i];
				}
			} else { // horizontal
				if (game.gameGrid[x + i][y] == null) {
					newCell = new CellCell(x + i, y);
					game.cells.add(newCell);
					game.gameGrid[x + i][y] = newCell;
				} else {
					newCell = (CellCell) game.gameGrid[x + i][y];
				}
			}
			newSum.addCell(newCell);
		}
	}

	private  void createSum(Game game, CellSum.Direction dir, int x, int y, int length, int sum) {
		CellSum newSum = new CellSum(dir, x, y, length, sum);
		addSumToGame(game, newSum, dir, x, y, length);
	}
	
	private  void addSumToGame(Game game, CellSum newSum, CellSum.Direction dir, int x, int y, int length) {
		game.gameGrid[x][y] = newSum;
		game.sums.add(newSum);
		for (int i = 1; i <= length; i++) {
			CellCell newCell;
			if (dir == CellSum.Direction.VERTICAL) {
				if (game.gameGrid[x][y + i] == null) {
					newCell = new CellCell(x, y + i);
					game.cells.add(newCell);
					game.gameGrid[x][y + i] = newCell;
				} else {
					newCell = (CellCell) game.gameGrid[x][y + i];
				}
			} else { // horizontal
				if (game.gameGrid[x + i][y] == null) {
					newCell = new CellCell(x + i, y);
					game.cells.add(newCell);
					game.gameGrid[x + i][y] = newCell;
				} else {
					newCell = (CellCell) game.gameGrid[x + i][y];
				}
			}
			newSum.addCell(newCell);
		}
	}

	//---------------------------------------------------------------
	// Sample games
	//---------------------------------------------------------------


	private  Game sampleGame1() {
		mainProgram.gameWidth = 9;
		mainProgram.gameHeight = 10;
		Game game = new Game(mainProgram.gameWidth, mainProgram.gameHeight);

		createSum(game, CellSum.Direction.VERTICAL, 1, 0, 3, 14);
		createSum(game, CellSum.Direction.VERTICAL, 2, 0, 4, 29);
		createSum(game, CellSum.Direction.VERTICAL, 4, 0, 5, 16);
		createSum(game, CellSum.Direction.VERTICAL, 5, 0, 3, 12);
		createSum(game, CellSum.Direction.VERTICAL, 7, 0, 5, 15);
		createSum(game, CellSum.Direction.VERTICAL, 8, 0, 4, 24);
		createSum(game, CellSum.Direction.VERTICAL, 3, 2, 4, 20);
		createSum(game, CellSum.Direction.VERTICAL, 6, 3, 4, 25);
		createSum(game, CellSum.Direction.VERTICAL, 5, 4, 5, 32);
		createSum(game, CellSum.Direction.VERTICAL, 1, 5, 4, 30);
		createSum(game, CellSum.Direction.VERTICAL, 2, 5, 4, 10);
		createSum(game, CellSum.Direction.VERTICAL, 4, 6, 3, 7);
		createSum(game, CellSum.Direction.VERTICAL, 7, 6, 3, 8);
		createSum(game, CellSum.Direction.VERTICAL, 8, 6, 3, 20);

		createSum(game, CellSum.Direction.HORIZONTAL, 0, 1, 2, 14);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 2, 2, 6);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 3, 5, 22);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 6, 3, 9);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 7, 2, 13);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 8, 2, 8);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 9, 2, 11);
		createSum(game, CellSum.Direction.HORIZONTAL, 1, 4, 3, 18);
		createSum(game, CellSum.Direction.HORIZONTAL, 2, 5, 5, 35);
		createSum(game, CellSum.Direction.HORIZONTAL, 3, 1, 2, 13);
		createSum(game, CellSum.Direction.HORIZONTAL, 3, 2, 2, 4);
		createSum(game, CellSum.Direction.HORIZONTAL, 3, 7, 5, 15);
		createSum(game, CellSum.Direction.HORIZONTAL, 3, 8, 2, 4);
		createSum(game, CellSum.Direction.HORIZONTAL, 3, 9, 2, 9);
		createSum(game, CellSum.Direction.HORIZONTAL, 4, 6, 2, 16);
		createSum(game, CellSum.Direction.HORIZONTAL, 5, 4, 3, 21);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 1, 2, 4);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 2, 2, 11);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 3, 2, 6);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 8, 2, 9);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 9, 2, 14);

		return game;
	}

	private  Game sampleGame2() {
		mainProgram.gameWidth = 9;
		mainProgram.gameHeight = 9;
		Game game = new Game(mainProgram.gameWidth, mainProgram.gameHeight);

		createSum(game, CellSum.Direction.VERTICAL, 2, 0, 3, 12);
		createSum(game, CellSum.Direction.VERTICAL, 3, 0, 8, 36);
		createSum(game, CellSum.Direction.VERTICAL, 5, 0, 2, 4);
		createSum(game, CellSum.Direction.VERTICAL, 6, 0, 8, 36);
		createSum(game, CellSum.Direction.VERTICAL, 7, 0, 4, 25);
		createSum(game, CellSum.Direction.VERTICAL, 1, 1, 2, 11);
		createSum(game, CellSum.Direction.VERTICAL, 8, 1, 2, 5);
		createSum(game, CellSum.Direction.VERTICAL, 4, 2, 3, 22);
		createSum(game, CellSum.Direction.VERTICAL, 5, 3, 3, 19);
		createSum(game, CellSum.Direction.VERTICAL, 2, 4, 4, 26);
		createSum(game, CellSum.Direction.VERTICAL, 1, 5, 2, 13);
		createSum(game, CellSum.Direction.VERTICAL, 7, 5, 3, 18);
		createSum(game, CellSum.Direction.VERTICAL, 8, 5, 2, 12);
		createSum(game, CellSum.Direction.VERTICAL, 4, 6, 2, 17);

		createSum(game, CellSum.Direction.HORIZONTAL, 0, 2, 3, 17);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 3, 4, 15);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 6, 3, 17);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 7, 4, 29);
		createSum(game, CellSum.Direction.HORIZONTAL, 1, 1, 2, 17);
		createSum(game, CellSum.Direction.HORIZONTAL, 1, 5, 5, 18);
		createSum(game, CellSum.Direction.HORIZONTAL, 1, 8, 3, 11);
		createSum(game, CellSum.Direction.HORIZONTAL, 2, 4, 5, 28);
		createSum(game, CellSum.Direction.HORIZONTAL, 4, 1, 3, 15);
		createSum(game, CellSum.Direction.HORIZONTAL, 4, 2, 4, 24);
		createSum(game, CellSum.Direction.HORIZONTAL, 4, 6, 4, 15);
		createSum(game, CellSum.Direction.HORIZONTAL, 5, 3, 3, 13);
		createSum(game, CellSum.Direction.HORIZONTAL, 5, 7, 3, 24);
		createSum(game, CellSum.Direction.HORIZONTAL, 5, 8, 2, 13);

		return game;
	}

	// TODO: Solver is not currently able to solve this puzzle!
	private  Game sampleGame3() {
		mainProgram.gameWidth = 10;
		mainProgram.gameHeight = 12;
		Game game = new Game(mainProgram.gameWidth, mainProgram.gameHeight);

		createSum(game, CellSum.Direction.VERTICAL, 1, 0, 2, 7);
		createSum(game, CellSum.Direction.VERTICAL, 2, 0, 2, 13);
		createSum(game, CellSum.Direction.VERTICAL, 3, 0, 4, 25);
		createSum(game, CellSum.Direction.VERTICAL, 5, 0, 2, 10);
		createSum(game, CellSum.Direction.VERTICAL, 6, 0, 2, 8);
		createSum(game, CellSum.Direction.VERTICAL, 8, 0, 8, 40);
		createSum(game, CellSum.Direction.VERTICAL, 9, 0, 4, 27);
		createSum(game, CellSum.Direction.VERTICAL, 4, 1, 2, 9);
		createSum(game, CellSum.Direction.VERTICAL, 7, 2, 4, 17);
		createSum(game, CellSum.Direction.VERTICAL, 1, 3, 2, 16);
		createSum(game, CellSum.Direction.VERTICAL, 2, 3, 8, 39);
		createSum(game, CellSum.Direction.VERTICAL, 6, 3, 3, 23);
		createSum(game, CellSum.Direction.VERTICAL, 5, 4, 3, 20);
		createSum(game, CellSum.Direction.VERTICAL, 3, 5, 4, 27);
		createSum(game, CellSum.Direction.VERTICAL, 4, 5, 3, 23);
		createSum(game, CellSum.Direction.VERTICAL, 9, 6, 2, 4);
		createSum(game, CellSum.Direction.VERTICAL, 1, 7, 4, 12);
		createSum(game, CellSum.Direction.VERTICAL, 7, 7, 4, 28);
		createSum(game, CellSum.Direction.VERTICAL, 6, 8, 2, 13);
		createSum(game, CellSum.Direction.VERTICAL, 4, 9, 2, 7);
		createSum(game, CellSum.Direction.VERTICAL, 5, 9, 2, 15);
		createSum(game, CellSum.Direction.VERTICAL, 8, 9, 2, 10);
		createSum(game, CellSum.Direction.VERTICAL, 9, 9, 2, 3);

		createSum(game, CellSum.Direction.HORIZONTAL, 0, 1, 3, 22);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 2, 6, 34);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 4, 3, 14);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 5, 2, 8);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 8, 4, 29);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 9, 3, 13);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 10, 2, 12);
		createSum(game, CellSum.Direction.HORIZONTAL, 0, 11, 2, 11);
		createSum(game, CellSum.Direction.HORIZONTAL, 1, 6, 7, 37);
		createSum(game, CellSum.Direction.HORIZONTAL, 1, 7, 4, 29);
		createSum(game, CellSum.Direction.HORIZONTAL, 2, 3, 2, 8);
		createSum(game, CellSum.Direction.HORIZONTAL, 3, 10, 6, 34);
		createSum(game, CellSum.Direction.HORIZONTAL, 3, 11, 2, 12);
		createSum(game, CellSum.Direction.HORIZONTAL, 4, 1, 2, 7);
		createSum(game, CellSum.Direction.HORIZONTAL, 4, 5, 4, 12);
		createSum(game, CellSum.Direction.HORIZONTAL, 5, 4, 4, 16);
		createSum(game, CellSum.Direction.HORIZONTAL, 5, 9, 2, 16);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 3, 3, 24);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 8, 3, 17);
		createSum(game, CellSum.Direction.HORIZONTAL, 6, 11, 3, 8);
		createSum(game, CellSum.Direction.HORIZONTAL, 7, 1, 2, 16);
		createSum(game, CellSum.Direction.HORIZONTAL, 7, 2, 2, 10);
		createSum(game, CellSum.Direction.HORIZONTAL, 7, 7, 2, 7);

		return game;
	}
	
	// A very simple game for testing...
	private Game sampleGame4() {
		mainProgram.gameWidth = 5;
		mainProgram.gameHeight = 5;
		Game game = new Game(mainProgram.gameWidth, mainProgram.gameHeight);
		
		createSum(game,CellSum.Direction.VERTICAL, 1,0,3,7);
		createSum(game,CellSum.Direction.VERTICAL, 2,0,3,8);
		createSum(game,CellSum.Direction.HORIZONTAL, 0, 1, 2, 5);
		createSum(game,CellSum.Direction.HORIZONTAL, 0, 2, 2, 5);
		createSum(game,CellSum.Direction.HORIZONTAL, 0, 3, 2, 5);
		
		return game;
	}

}
