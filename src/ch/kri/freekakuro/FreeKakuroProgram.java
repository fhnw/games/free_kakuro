/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;



public class FreeKakuroProgram extends JFrame {
	private FreeKakuro kakuro;
	
	public static void main(String[] args) {
		new FreeKakuroProgram(); 
	}
	
	public FreeKakuroProgram() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((screenSize.width - FreeKakuro.appWidth) / 2, (screenSize.height - FreeKakuro.appHeight) / 2, FreeKakuro.appWidth, FreeKakuro.appHeight);

		setTitle("Free Kakuro");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setResizable(true);
		kakuro = new FreeKakuro(this);
		this.setLayout(new BorderLayout());
		this.add(kakuro.getGUI(), BorderLayout.CENTER);
		this.addWindowListener(new WindowAdapter() {
			public void windowIconified(WindowEvent we) {
				kakuro.startPause();
			}
			public void windowDeiconified(WindowEvent we) {
				kakuro.stopPause();
			}
		});
		
		this.setVisible(true);
	}

}
