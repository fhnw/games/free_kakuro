/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JPanel;

import ch.kri.freekakuro.data_classes.CellCell;
import ch.kri.freekakuro.data_classes.Game;
import ch.kri.freekakuro.data_classes.PossibleValues;
import ch.kri.freekakuro.data_classes.CellSum;
import ch.kri.freekakuro.display_classes.DisplayCell;

public class FreeKakuro {
	// Global constants
	public static final int appWidth = 450;
	public static final int appHeight = 600;

	public static final int majorVersion = 0;
	public static final int minorVersion = 0;
	public static final boolean logEnabled = true;
	public static final String defLang = "en";

	public static final Color BLUE_CLEAR = new Color(204, 252, 252);
	public static final Color BLUE_MED = new Color(160, 160, 200);
	public static final Color GRAY_CLEAR = new Color(200, 220, 220);
	public static final Color GREEN_DARK = new Color(20, 140, 20);
	public static final Color RED_DARK = new Color(140, 20, 20);
	public static final Color RED_CLEAR = new Color(255, 150, 120);

	// Game interface and data
	private FreeKakuroGui gui;
	Game game;
	GameSolver solver;

	// Specific game info from the start-dialog
	public Integer gameWidth = 10;
	public Integer gameHeight = 12;
	public Integer gameDifficulty = 1;
	public Integer gameNumber = 0;
	public boolean gameSymmetrical = false;
	public boolean startGame = false;
	
	// Misc. instance variables
	private Date startTime;
	private Date pauseStarted;
	private long totalPauseTime;

	public FreeKakuro(Object parentFrame) {
		pauseStarted = null;
		totalPauseTime = 0;
		gui = new FreeKakuroGui(this, parentFrame);
	}

	public void startPause() {
		pauseStarted = new Date();
		FreeKakuro.log("pause started");
	}

	public void stopPause() {
		Date pauseStopped;
		long pauseTime;

		// If pauseStarted has a value, then we are resuming after a pause,
		// and need to deduct the pause-time from the accumulated time in
		// the game. If pauseStarted is null, then we are just starting up
		// for the first time, and have nothing to do.
		if (pauseStarted != null) {
			pauseStopped = new java.util.Date();
			pauseTime = pauseStopped.getTime() - pauseStarted.getTime();
			totalPauseTime += pauseTime;
			pauseStarted = null;
			FreeKakuro.log("pause ended");
		}
	}

	public JPanel getGUI() {
		return gui;
	}

	// A global debugging function
	public static void log(String s) {
		if (logEnabled) System.out.println(s);
	}
}
