/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.display_classes;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import ch.kri.freekakuro.data_classes.CellSum;

public class DisplayCellSum extends DisplayCell {
	private CellSum hSum;
	private CellSum vSum;

	public DisplayCellSum(int x, int y, CellSum sum) {
		super(x, y);
		setMargin(smallCellMargin);
		setFont(new Font(FONT_NAME, Font.BOLD, SMALL_TEXT_SIZE));
		this.setBackground(Color.LIGHT_GRAY);
		addSum(sum);
	}

	// Currenty very sensitive to the system font!!
	public void addSum(CellSum s) {
		if (s.getDirection() == CellSum.Direction.HORIZONTAL) {
			if (hSum != null) {
				throw new IllegalArgumentException();
			} else {
				hSum = s;
			}
		} else { // vertical
			if (vSum != null) {
				throw new IllegalArgumentException();
			} else {
				vSum = s;
			}
		}
		String text = "    ";
		if (hSum != null) {
			text += hSum.getSum();
		}
		text += "\n";
		if (vSum != null) {
			text += vSum.getSum();
		}
		this.setText(text);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.BLACK);
		g.drawLine(0,0,this.getWidth(), this.getHeight());
	}
}
