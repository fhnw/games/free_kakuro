/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.display_classes;

import java.awt.Color;
import java.awt.Font;

import ch.kri.freekakuro.data_classes.CellSum;

public class DisplayCellEmpty extends DisplayCell {
	public DisplayCellEmpty(int x, int y) {
		super(x, y);
    setFont(new Font( FONT_NAME, Font.BOLD, SMALL_TEXT_SIZE));
    this.setBackground(Color.LIGHT_GRAY);
	}

	public void addSum(CellSum s) {
		// TODO Auto-generated method stub
		
	}
}
