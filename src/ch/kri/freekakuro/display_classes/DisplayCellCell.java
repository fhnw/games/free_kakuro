/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.display_classes;

import java.awt.Color;
import java.awt.Font;

import ch.kri.freekakuro.data_classes.CellCell;

public class DisplayCellCell extends DisplayCell {
	private CellCell cell;
	private String userText;
	
	public DisplayCellCell(int x, int y, CellCell cell) {
		super(x, y);
		this.cell = cell;		
		this.setEditable(true);
		userText = "";
		setMargin(bigCellMargin);
    setFont(new Font( FONT_NAME, Font.BOLD, BIG_TEXT_SIZE));
	}
	
	public CellCell getGameCell() {
		return cell;
	}
	
	@Override
	/**
	 * General text setting - text entered by the user
	 */
	public void setText(String text) {
		if (text == null) {
			text = "";
		}
		userText = text;
		setBackground(Color.WHITE);
		if (text.length() > 1) {
			setMargin(smallCellMargin);
	    setFont(new Font( FONT_NAME, Font.BOLD, SMALL_TEXT_SIZE));
		} else {
			setMargin(bigCellMargin);
	    setFont(new Font( FONT_NAME, Font.BOLD, BIG_TEXT_SIZE));
		}
		super.setText(text);
	}
	
	/**
	 * Instruct the cell to display the possible values. This is
	 * only done if the user has not entered a single value of
	 * his own. If the user has entered nothing, or something other
	 * than a single digit, the user text is temporarily hidden.
	 */
	public void showPossVals() {
		if (!userSolution()) {
			setBackground(Color.PINK);
			setMargin(smallCellMargin);
	    setFont(new Font( FONT_NAME, Font.BOLD, SMALL_TEXT_SIZE));
	    super.setText(cell.getPossibleValues().toString());
		}
	}
	
	/**
	 * Instruct the cell to display user-entered text
	 */
	public void showUserText() {
		setText(userText);
	}
	
	/**
	 * Determine whether the user-entered text is a single digit
	 */
	private boolean userSolution() {
		boolean value = false;
		if (userText.length() == 1) {
			char c = userText.charAt(0);
			if (c >= '1' & c <= '9') {
				value = true;
			}
		}
		return value;
	}
}
