/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.display_classes;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import ch.kri.freekakuro.FreeKakuro;

public abstract class DisplayCell extends JTextArea {
	public static final String FONT_NAME = "SansSerif";
	public static final int SMALL_TEXT_SIZE = 10;
	public static final int BIG_TEXT_SIZE = 24;
	
	public static final Insets bigCellMargin = new Insets(3, 13, 9, 9);
	public static final Insets smallCellMargin = new Insets(5, 5, 5, 5);
	private int x;
	private int y;

	public DisplayCell(int x, int y) {
		super();
		this.x = x;
		this.y = y;

		Dimension dim = new Dimension(38, 38);
		this.setMaximumSize(dim);
		this.setMinimumSize(dim);
		this.setPreferredSize(dim);
		this.setSize(dim);
		
		setEditable(false);
		setBackground(Color.WHITE);
		setForeground(Color.BLACK);
		setLineWrap(true);
		setWrapStyleWord(false);
	}
}
