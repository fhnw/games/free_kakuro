/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.test;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.kri.freekakuro.data_classes.CellCell;
import ch.kri.freekakuro.data_classes.CellSum;

/**
 * @author Administrator
 *
 */
public class CellCellTest {
	CellCell a, b, c, d, e, f, g;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		a = new CellCell(1, 1);
		b = new CellCell(1, 2);
		c = new CellCell(1, 3);
		d = new CellCell(2, 1);
		e = new CellCell(3, 1);
		f = new CellCell(4, 1);
		g = new CellCell(5, 1);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		a = null;
		b = null;
		c = null;
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#CellCell(int, int)}.
	 */
	@Test
	public final void testCellCell() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#isUnique()}.
	 */
	@Test
	public final void testIsUnique() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#isImpossible()}.
	 */
	@Test
	public final void testIsImpossible() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#getUniqueValue()}.
	 */
	@Test
	public final void testGetUniqueValue() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#addPossibleValue(java.lang.Integer)}.
	 */
	@Test
	public final void testAddPossibleValue() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#removePossibleValue(java.lang.Integer)}.
	 */
	@Test
	public final void testRemovePossibleValue() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#getPossibleValues()}.
	 */
	@Test
	public final void testGetPossibleValues() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#setValue(java.lang.Integer)}.
	 */
	@Test
	public final void testSetValue() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#getValue()}.
	 */
	@Test
	public final void testGetValue() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#getSums()}.
	 */
	@Test
	public final void testGetSums() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.CellCell#addSum(ch.kri.freekakuro.data_classes.CellSum)}.
	 */
	@Test
	public final void testAddSum() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Unavoidable a combined test of sums and cells. We create two sums that intersect at a cell, and
	 * check that the intersection is correct.
	 */
	@Test
	public void testSum() {
		CellSum sum8 = new CellSum(CellSum.Direction.VERTICAL,1, 1, 3, 8);
		TestCase.assertEquals(15, sum8.getPossibleValues().sum());
		CellSum sum34 = new CellSum(CellSum.Direction.HORIZONTAL,1, 1, 5, 34);
		TestCase.assertEquals(34, sum34.getPossibleValues().sum());

		sum8.addCell(a);
		sum8.addCell(b);
		sum8.addCell(c);

		sum34.addCell(a);
		sum34.addCell(d);
		sum34.addCell(e);
		sum34.addCell(f);
		sum34.addCell(g);

		TestCase.assertTrue(a.isUnique());
		TestCase.assertEquals(new Integer(4), a.getUniqueValue());
	}

}
