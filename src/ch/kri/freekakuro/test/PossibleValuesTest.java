/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */package ch.kri.freekakuro.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.kri.freekakuro.data_classes.CellSum;
import ch.kri.freekakuro.data_classes.PossibleValues;

/**
 * @author Administrator
 *
 */
public class PossibleValuesTest extends TestCase {
	PossibleValues a, b;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		a = new PossibleValues(1, 9);
		b = new PossibleValues(1, 5);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#getPatternsForSum(ch.kri.freekakuro.data_classes.CellSum)}.
	 */
	@Test
	public final void testGetPatternsForSum() {
		CellSum sum = new CellSum(CellSum.Direction.HORIZONTAL,1,1,3,8);
		ArrayList<PossibleValues> patterns = PossibleValues.getPatternsForSum(sum);
		assertEquals(2, patterns.size());
		int valueSum = 0;
		for (PossibleValues pattern : patterns) {
			valueSum += pattern.sum();
		}
		assertEquals(16, valueSum);
		
		sum = new CellSum(CellSum.Direction.HORIZONTAL,1,1,9,45);
		patterns = PossibleValues.getPatternsForSum(sum);
		assertEquals(1, patterns.size());
		valueSum = 0;
		for (PossibleValues pattern : patterns) {
			valueSum += pattern.sum();
		}
		assertEquals(45, valueSum);
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#intersection(ch.kri.freekakuro.data_classes.PossibleValues[])}.
	 */
	@Test
	public final void testIntersectionPossibleValuesArray() {
		PossibleValues ab = PossibleValues.intersection(a,b);
		TestCase.assertEquals(1, ab.size());
		TestCase.assertEquals(1, ab.sum());
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#intersection(java.util.ArrayList)}.
	 */
	@Test
	public final void testIntersectionArrayListOfPossibleValues() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#union(ch.kri.freekakuro.data_classes.PossibleValues[])}.
	 */
	@Test
	public final void testUnionPossibleValuesArray() {
		PossibleValues ab = PossibleValues.union(a,b);
		TestCase.assertEquals(3, ab.size());
		TestCase.assertEquals(15, ab.sum());
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#union(java.util.ArrayList)}.
	 */
	@Test
	public final void testUnionArrayListOfPossibleValues() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#PossibleValues()}.
	 */
	@Test
	public final void testPossibleValues() {
		PossibleValues p = new PossibleValues();
		p.add(3);
		p.add(7);
		p.add(10);
		p.add(-1);
		p.add(0);
		p.remove(7);
		p.remove(5);
		TestCase.assertEquals(3, p.sum());
		TestCase.assertEquals(1, p.size());
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#PossibleValues(java.lang.Integer[])}.
	 */
	@Test
	public final void testPossibleValuesIntegerArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#PossibleValues(ch.kri.freekakuro.data_classes.PossibleValues)}.
	 */
	@Test
	public final void testPossibleValuesPossibleValues() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#equals(ch.kri.freekakuro.data_classes.PossibleValues)}.
	 */
	@Test
	public final void testEqualsPossibleValues() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#add(java.lang.Integer)}.
	 */
	@Test
	public final void testAddInteger() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#sum()}.
	 */
	@Test
	public final void testSum() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#toString()}.
	 */
	@Test
	public final void testToString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#getPatternKey(int, int)}.
	 */
	@Test
	public final void testGetPatternKey() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link ch.kri.freekakuro.data_classes.PossibleValues#createPatterns(int, int)}.
	 */
	@Test
	public final void testCreatePatterns() {
		fail("Not yet implemented"); // TODO
	}

}
