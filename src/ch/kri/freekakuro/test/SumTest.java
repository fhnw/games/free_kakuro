/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.test;

import ch.kri.freekakuro.data_classes.CellCell;
import ch.kri.freekakuro.data_classes.CellSum;
import junit.framework.TestCase;

public class SumTest extends TestCase {
	CellCell a, b, c, d, e, f, g;

	@Override
	public void setUp() {
		a = new CellCell(1, 1);
		b = new CellCell(1, 2);
		c = new CellCell(1, 3);
		d = new CellCell(2, 1);
		e = new CellCell(3, 1);
		f = new CellCell(4, 1);
		g = new CellCell(5, 1);
	}

	@Override
	public void tearDown() {
		a = null;
		b = null;
		c = null;
	}

	/**
	 * Unavoidable a combined test of sums and cells. We create two sums that intersect at a cell, and
	 * check that the intersection is correct.
	 */
	public void testSum() {
		CellSum sum8 = new CellSum(CellSum.Direction.VERTICAL,1, 1, 3, 8);
		TestCase.assertEquals(15, sum8.getPossibleValues().sum());
		CellSum sum34 = new CellSum(CellSum.Direction.HORIZONTAL,1, 1, 5, 34);
		TestCase.assertEquals(34, sum34.getPossibleValues().sum());

		sum8.addCell(a);
		sum8.addCell(b);
		sum8.addCell(c);

		sum34.addCell(a);
		sum34.addCell(d);
		sum34.addCell(e);
		sum34.addCell(f);
		sum34.addCell(g);

		TestCase.assertTrue(a.isUnique());
		TestCase.assertEquals(new Integer(4), a.getUniqueValue());
	}

}
