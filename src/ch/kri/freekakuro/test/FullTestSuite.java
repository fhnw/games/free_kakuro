package ch.kri.freekakuro.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CellCellTest.class,PossibleValuesTest.class})

public class FullTestSuite {

}
