package ch.kri.freekakuro.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for ch.kri.freekakuro.test");
		//$JUnit-BEGIN$
		suite.addTestSuite(PossibleValuesTest.class);
		suite.addTestSuite(SumTest.class);
		//$JUnit-END$
		return suite;
	}

}
