/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.translation;

import java.util.EnumSet;

/**
 * An enumerated list of all translatable components. When
 * the language is changed, the text and tool-tip (if applicable)
 * for each of these controls will be changed.
 *
 */
public enum Translatable {
		// items from the main window
		MenuFile, MenuFileNew, MenuFilePrint, MenuFileExit,
		MenuLanguage, MenuHelp, MenuHelpHelp, MenuHelpAbout,
		
		// items from the new-game dialog
		NewGameWidth, NewGameHeight, NewGameDifficulty, NewGameSymmetrical,
		NewGameGameNumber, NewGameStart

		;

		public static EnumSet<Translatable> MainWindow = EnumSet.range(MenuFile, MenuHelpAbout);
		public static EnumSet<Translatable> NewDialog = EnumSet.range(NewGameWidth, NewGameStart);
}
