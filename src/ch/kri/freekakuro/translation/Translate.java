/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.translation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Translate {

	private static final Locale[] locales = new Locale[] { new Locale("en"), new Locale("de"), };

	private String baseBundlePath;
	private ResourceBundle resourceBundle;
	
	private EnumMap<Translatable, ArrayList<JComponent>> translatableControls;

	// Constructor requires the base path to use...
	public Translate(String baseBundlePath) {
		init(baseBundlePath);
		resourceBundle = ResourceBundle.getBundle(baseBundlePath, Locale.getDefault());
	}
	
	// Constructor requires the base path to use...
	public Translate(String baseBundlePath, Locale locale) {
		init(baseBundlePath);
		resourceBundle = ResourceBundle.getBundle(baseBundlePath, locale);
	}

	// Constructor requires the base path to use...
	public Translate(String baseBundlePath, String  localeString) {
		init(baseBundlePath);
		Locale locale = Locale.getDefault();
		for (int i = 0; i < locales.length; i++) {
			String tmpLang = locales[i].getLanguage();
			if (localeString.substring(0, tmpLang.length()).equals(tmpLang) ) {
				locale = locales[i];
				break;
			}
		}
		resourceBundle = ResourceBundle.getBundle(baseBundlePath, locale);
	}

	private void init(String baseBundlePath) {
		this.baseBundlePath = baseBundlePath;
		translatableControls = new EnumMap<Translatable, ArrayList<JComponent>>(Translatable.class);
	}
	/**
	 * List the locales supported by the application, as Locale objects
	 */
	public static Locale[] getAvailableLocales() {
		return locales;
	}

	/**
	 * List the locales supported by the application, as Strings, and in their own
	 * languages
	 */
	public static String[] getAvailableLocaleNames() {
		String names[] = new String[locales.length];
		for (int i = 0; i < locales.length; i++) {
			names[i] = locales[i].getDisplayLanguage(locales[i]);
		}
		return names;
	}

	/**
	 * Set the locale (invalid locales will default ultimately to english)
	 */
	public void setLocale(Locale locale) {
		resourceBundle = ResourceBundle.getBundle(baseBundlePath, locale);
	}

	/**
	 * Public method to get string resources, default to "--" *
	 */
	public String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return "--";
		}
	}

	/**
	 * Public method to set both the text and the tooltip for a control
	 */
	public void setStrings(JComponent control, String key) {
		Class<?> controlClass = control.getClass();
		control.setToolTipText(getString(key + ".tooltip"));

		// We explicitly check for control-types that have a setText method, and
		// for which we actually want to set the text.
		if ((controlClass.equals(JMenu.class)) | (controlClass.equals(JMenuItem.class))
				| (controlClass.equals(JLabel.class)) | (controlClass.equals(JCheckBox.class))
				| (controlClass.equals(JButton.class)) | (controlClass.equals(JMenu.class))) {
			Class<?>[] methodParmTypes = new Class[] { String.class };
			Object[] methodParms = new Object[] { getString(key) };
			Method callSetText;
			try {
				callSetText = control.getClass().getMethod("setText", methodParmTypes);
				callSetText.invoke(control, methodParms);
			} catch (Exception e) {}
		}
	}
	
	public void put(Translatable t, JComponent c) {
		ArrayList<JComponent> list =translatableControls.get(t); 
		if (list == null) {
			list = new ArrayList<JComponent>();
			translatableControls.put(t, list);
		}
		list.add(c);
	}
	
	public void setGuiTexts(EnumSet<Translatable> elements) {
		for (Iterator<Translatable> i = elements.iterator(); i.hasNext();) {
			Translatable t = i.next();
			ArrayList<JComponent> list =translatableControls.get(t);
			if (list != null) {
				for (Iterator<JComponent> j = list.iterator(); j.hasNext();) {
					JComponent control = j.next();
					setStrings(control, t.name());
				}
			}
		}
	}

}
