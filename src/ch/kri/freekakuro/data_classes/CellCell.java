/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.data_classes;

import java.util.ArrayList;

import ch.kri.freekakuro.display_classes.DisplayCell;

/**
 * Represents a single cell that contains a number.
 * 
 * @author Brad Richards
 *
 */
public class CellCell extends Cell {
	private PossibleValues possibleValues;
	private ArrayList<CellSum> sums; // the sums this cell is part of
	private DisplayCell displayCell; // the displayCell showing this cell (null if not displayed)
	private Integer value; // the value of this cell in the final solution (not the user's entries!)
	
	public CellCell(int x, int y) {
		super(x,y);
		sums = new ArrayList<CellSum>();
		value = null;
	}
	
	/**
	 * Returns true if the cell is a single possible value, i.e.,
	 * its value has been found.
	 * 
	 * @return true if the cell's value is known
	 */
	public boolean isUnique() {
		return (possibleValues.size() == 1);
	}

	/**
	 * Returns true if the cell has no possible value, i.e.,
	 * all possible values have been eliminated.
	 * 
	 * @return true if the cell has no possible value
	 */
	public boolean isImpossible() {
		return possibleValues.isEmpty();
	}
	
	/**
	 * Returns the unique possible value of this cell. If there is no
	 * unique value, then this method returns null.
	 * 
	 * @return Integer the value of the cell, or null if not known.
	 */
	public Integer getUniqueValue() {
		if (isUnique()) {
			return possibleValues.first();
		} else {
			return null;
		}
	}

	public void addPossibleValue(Integer possValue) {
		possibleValues.add(possValue);
	}
	
	public void removePossibleValue(Integer nonValue) {
		possibleValues.remove(nonValue);
	}
	
	public PossibleValues getPossibleValues() {
		return possibleValues;
	}
	
	public void setPossibleValues(PossibleValues pv) {
		possibleValues = pv;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
	public Integer getValue() {
		return value;
	}
	public ArrayList<CellSum> getSums() {
		return sums;
	}
	
	/**
	 * Add this cell to a new sum. We simultaneously restrict the possible
	 * values of this cell to values that can be part of the sum.
	 * 
	 * @param newSum
	 */
	public void addSum(CellSum newSum) {
		if (sums.size() >= 2) {
			throw new IllegalArgumentException();
		}
		sums.add(newSum);
		if (possibleValues == null) { // first of two sums - get the possible values
			possibleValues = new PossibleValues(newSum.getPossibleValues());
		} else { // second of two sums - intersect the possible values
			possibleValues.retainAll(newSum.getPossibleValues());
		}
	}
	
//	/**
//	 * After substantial modifications by the solver, cells may be told to
//	 * update their possible values.
//	 */
//	public void xrecalculatePossibleValues() {
//		possibleValues = new PossibleValues(sums.get(0).getPossibleValues());
//		if (sums.size() == 2) {
//			possibleValues.retainAll(sums.get(1).getPossibleValues());
//		}
//	}
}
