/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.data_classes;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The game data is a set of sums and a set of cells, in an obviously rather intricate relationship.
 * We use this trivial class to group the two together into a single object.
 * 
 * In addition, we store the set of all possible patterns here, in a hash-table sorted by an integer
 * key constructed from sum-length * 100 + sum-value. For example, the pattern 1,2,4 for the sum 7
 * of length three would be stored under the key 307. The only pattern for a sum of length 9 is
 * stored under the key 945.
 * 
 * The gameGrid is an array where the cells are stored as they are created. This serves to identify
 * when cells already exist, and do not need to be created. Important note: only value cells (class
 * CellCell) are stored in the grid. The sum headers are stored, but overwrite each other - and
 * hence are not a reliable part of the game representation.
 * 
 * @author Brad Richards
 * 
 */
public class Game {
	public int width, height;
	public ArrayList<CellSum> sums;
	public ArrayList<CellCell> cells;
	public HashMap<Integer, ArrayList<PossibleValues>> patterns;
	public Cell[][] gameGrid; // See important not in class header!

	public Game(int width, int height) {
		this.width = width;
		this.height = height;
		sums = new ArrayList<CellSum>();
		cells = new ArrayList<CellCell>();
		gameGrid = new Cell[width][height];
		initPatterns();
	}
	
	/**
	 * Initialize all possible patterns - including patterns of length 1
	 */
	private void initPatterns() {
		patterns = new HashMap<Integer, ArrayList<PossibleValues>>();
		
		for (int size = 1; size <= 9; size++) {
			int minSum = size * (size+1) / 2;
			int maxSum = size * 10 - minSum;
			for (int sum = minSum ; sum <= maxSum; sum++) {
				int patternKey = PossibleValues.getPatternKey(size, sum);
				patterns.put(patternKey, PossibleValues.createPatterns(size, sum));
			}
		}
	}
}
