/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.data_classes;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Represents a sum comprised of two to nine cells.
 * 
 * Note that we explicitly represent size - the number of cells in the
 * sum. It may seem that this would be the same as cells.size(). However,
 * when we are first constructing a sum, we will be adding the cells,
 * and it will be useful to know what the final size of the sum will be.
 * 
 * @author Brad Richards
 *
 */
public class CellSum extends Cell {
	public enum Direction {
		VERTICAL, HORIZONTAL
	}
	private Direction dir;
	private ArrayList<CellCell> cells;
	private int size; // the number of cells
	private int sum; // the total sum of the cells
	private ArrayList<PossibleValues> patterns; // the patterns possible for the sum (continually reduced as cells become definite)
	private PossibleValues possibleValues; // the possible values for the open cells remaining in the sum

	// constructor used when the size and sum are both known,
	// i.e., for sample games
	public CellSum(Direction dir, int x, int y, int size, int sum) {
		super(x, y);
		this.dir = dir;
		if (size < 2 | size > 9) {
			throw new IllegalArgumentException();
		}
		this.size = size;
		this.sum = sum;
		patterns = PossibleValues.getPatternsForSum(this);
		updatePossibleValues();
		cells = new ArrayList<CellCell>();
	}
	
	// constructor used when the size is known, but sum is not,
	// i.e., for games we are creating
	public CellSum(Direction dir, int x, int y, int size) {
		super(x, y);
		this.dir = dir;
		if (size < 2 | size > 9) {
			throw new IllegalArgumentException();
		}
		this.size = size;
		possibleValues = new PossibleValues(1,2,3,4,5,6,7,8,9);
		cells = new ArrayList<CellCell>();
	}

	public int getSum() {
		return sum;
	}
	
	// The sum is only set with this method when the constructor
	// without a sum has been used. This is used for creating games;
	// the sum will be set, and the (partial) game checked for
	// validity. The sum may be set multiple times before a valid
	// game can be developed.
	public void setSum(int sum) {
		this.sum = sum;
		patterns = PossibleValues.getPatternsForSum(this);
		updatePossibleValues();
	}
	
	public int getSize() {
		return size;
	}
	
	public ArrayList<CellCell> getCells() {
		return cells;
	}
	
	public ArrayList<PossibleValues> getPatterns() {
		return patterns;
	}
	
	public PossibleValues getPossibleValues() {
		return new PossibleValues(possibleValues);
	}
	
	/**
	 * Add a cell to this sum. This method also adds the sum to the cell.
	 * @param cell The cell to add
	 */
	public void addCell(CellCell cell) {
		if (cells.size() < size) {
			cells.add(cell);
			cell.addSum(this);
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * This sum is being informed that a value has been committed to one of
	 * the cells it contains. We update our possible values, which involves
	 * three steps:
	 * - eliminate any patterns that do not contain the committed value
	 * - update the possible values as the union of all remaining patterns
	 * - remove the committed value (all committed values) from our possible values
	 * - force all uncommitted cells to update their possible values 
	 * @param value
	 */
	public void valueCommitted(int value) {
		// eliminate any patterns that do not contain the committed value. We also accumulate
		// all values that appear in these, and in the other PossibleValue sets, so that we
		// can eliminate any other possible values that occur only in the eliminated poss-val sets
		PossibleValues pv_remaining = new PossibleValues();
		PossibleValues pv_eliminated = new PossibleValues();
		for (Iterator<PossibleValues> i = patterns.iterator(); i.hasNext();) {
			PossibleValues pv = i.next();
			if (!pv.contains(value)) {
				pv_eliminated.addAll(pv);
				i.remove();
			} else {
				pv_remaining.addAll(pv);
			}
		}
		
		// remove any possible values that occurred only in the eliminated patterns
		pv_eliminated.removeAll(pv_remaining);
		this.possibleValues.removeAll(pv_eliminated);
		
		// remove the committed value (all committed values) from our possible values
		for (CellCell cell : cells) {
			Integer cellValue = cell.getValue();
			if (cellValue != null) {
				possibleValues.remove(cellValue);
			}
		}
		
		// restrict the possible values in all cells 
		for (CellCell cell : cells) {
			cell.getPossibleValues().retainAll(possibleValues);
		}
	}
	
	public Direction getDirection() {
		return dir;
	}
	
	private void updatePossibleValues() {
		possibleValues = new PossibleValues();
		for (PossibleValues p : patterns) {
			possibleValues.addAll(p);
		}
	}
}
