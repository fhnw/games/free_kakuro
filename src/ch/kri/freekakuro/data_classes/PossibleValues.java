/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro.data_classes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * A set of possible values is used to represent various things. It can be
 * <ul>
 * <li>The values possible in a single cell</li>
 * <li>The values possible anywhere in a sum</li>
 * <li>A pattern - a set of values which add up to a sum</li>
 * </ul>
 * 
 * Example: consider the sum 8, to be achieved in 3 cells. This value can
 * be reached in two possible ways: 1,2,5 or 1,3,4. Each of these is a pattern,
 * represented as a set of possible values. The possible values over the entire
 * sum is the union of these two, or 1,2,3,4,5.
 * 
 * Possible value sets may be combined in various
 * ways. Given a set of patterns for a sum, the possible values for the sum
 * is the union of the possible values for each pattern. The possible values
 * of a cell is the intersection of the possible values for the two sums it
 * is a part of.
 * 
 * @author Brad Richards
 *
 */
public class PossibleValues extends TreeSet<Integer> {
	public static ArrayList<PossibleValues> getPatternsForSum(CellSum s) {
		return PossibleValues.createPatterns(s.getSize(), s.getSum());
	}
	
	public static PossibleValues intersection(PossibleValues... pp) {
		PossibleValues intersection = new PossibleValues();
		if (pp.length > 0) {
			intersection.addAll(pp[0]);
		}
		
		for (int i = 1; i < pp.length; i++) {
			intersection.retainAll(pp[i]);
		}
		return intersection;
	}

	public static PossibleValues intersection(ArrayList<PossibleValues> pp) {
		PossibleValues intersection = new PossibleValues();
		if (pp.size() > 0) {
			intersection.addAll(pp.get(0));
		}
		
		for (int i = 1; i < pp.size(); i++) {
			intersection.retainAll(pp.get(i));
		}
		return intersection;
	}

	public static PossibleValues union(PossibleValues... pp) {
		PossibleValues union = new PossibleValues();
		for (PossibleValues p : pp) {
			union.addAll(p);
		}
		return union;
	}
	
	public static PossibleValues union(ArrayList<PossibleValues> pp) {
		PossibleValues union = new PossibleValues();
		for (PossibleValues p : pp) {
			union.addAll(p);
		}
		return union;
	}
	
	public PossibleValues() {
		super();
	}
	
	public PossibleValues(Integer... values) {
		super();
		for (Integer i : values) {
			this.add(i);
		}
	}
	
	public PossibleValues(PossibleValues toCopy) {
		super();
		this.addAll(toCopy);
	}
	
	public boolean equals(PossibleValues p) {
		if (this.size() != p.size()) {
			return false;
		} else {
			PossibleValues i = PossibleValues.intersection(this, p);
			return (this.size() == i.size());
		}
	}
	
	@Override
	/**
	 * Add a new value to the set, if not already present. Values
	 * are restricted to the range 1-9 inclusive.
	 */
	public boolean add(Integer i) {
		if (i <=0 | i >= 10) {
			return false;
		} else {
			return super.add(i);
		}
	}
	
	/**
	 * Calculate the sum of all values in the set
	 * 
	 * @return the sum of the values
	 */
	public int sum() {
		int sum = 0;
		for (Integer i : this) {
			sum += i;
		}
		return sum;
	}
	
	@Override
	/**
	 * Convert the list of possible values to a string of digits
	 * @return the string of digits
	 */
	public String toString() {
		String possVals = "";
		for (Iterator<Integer> i = this.iterator(); i.hasNext();) {
			Integer possVal = i.next();
			possVals += possVal.toString();
		}
		return possVals;
	}
	
	/**
	 * Construct a key into the list of patterns. The key is sum-length * 100 + sum-value. For
	 * example, the pattern 1,2,4 for the sum 7 of length three would be stored under the key 307.
	 * The only pattern for a sum of length 9 is stored under the key 945.
	 * 
	 * @param size
	 *          Number of cells in the sum
	 * @param sum
	 *          Total sum of the cells
	 * @return
	 */
	public static int getPatternKey(int size, int value) {
		return size * 100 + value;
	}

	/**
	 * This is a recursive method used to generate all possible patterns that
	 * can lead to a particular sum.
	 * 
	 * The recursive case places one additional number, add this to the list
	 * of values used, and decrements the remaining size (number of values)
	 * left to be placed. For efficiency, the recursive cases also checks to
	 * see if we have reached an impossible state (for example, if the remaining
	 * sum is negative).
	 * 
	 * The base case occurs when the number of remaining values is 1. At this
	 * point, either the remaining required sum is possible (range 1 to 9, and
	 * not in usedValues) or it is not possible.
	 * 
	 * @param patterns The accumulating list of possible patterns
	 * @param start The first value to use in the loop
	 * @param usedValues The values used so far in creating the current pattern
	 * @param size the remaining number of values to add to the current pattern
	 * @param sum the remaining sum to be added to the current pattern
	 * @return
	 */
	public static ArrayList<PossibleValues> createPatterns(int size, int sum) {
		ArrayList<PossibleValues> patterns = new ArrayList<PossibleValues>();
		PossibleValues usedValues = new PossibleValues();
		PossibleValues.createPatterns(patterns, 1, usedValues, size, sum);
		return patterns;
	}
	private static void createPatterns(ArrayList<PossibleValues> patterns, int start, PossibleValues usedValues, int size, int sum) {
		if (size == 1) { // base case
			if (sum >= 1 && sum <= 9  && sum >= start && !usedValues.contains(sum)) { // success, add new pattern
				PossibleValues newUsedValues = new PossibleValues(usedValues);
				newUsedValues.add(sum);
				patterns.add(new PossibleValues(newUsedValues));
			}
		} else { // recursive case
			for (int i = start; i <= 9; i++) {
				if (!usedValues.contains(i)) {
					PossibleValues newUsedValues = new PossibleValues(usedValues);
					newUsedValues.add(i);
					int newStart = i+1;
					int newSum = sum - i;
					int newSize = size - 1;
					if (newSum > 0) {
						createPatterns(patterns, newStart,newUsedValues,newSize,newSum);
					}
				}
			}
		}
	}
}
