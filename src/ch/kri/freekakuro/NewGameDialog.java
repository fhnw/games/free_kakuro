package ch.kri.freekakuro;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import ch.kri.freekakuro.translation.Translatable;
import ch.kri.freekakuro.translation.Translate;

public class NewGameDialog extends JDialog {
	private FreeKakuro mainProgram;
	private Translate translator;

	private JComboBox cmbWidth;
	private JComboBox cmbHeight;
	private JComboBox cmbDifficulty;
	private JTextField txtGameNumber;
	private JCheckBox chkSymmetrical;

	public static void showDialog(FreeKakuro mainProgram) {
		NewGameDialog ngd = new NewGameDialog(mainProgram);
		ngd.setVisible(true);
	}

	public NewGameDialog(FreeKakuro mainProg) {
		super((java.awt.Frame) null, "", true);
		this.mainProgram = mainProg;
		final NewGameDialog thisDialog = this; // for the action-listeners
		this.translator = new Translate("ch.kri.freekakuro.translation.FreeKakuro");
		this.setTitle(translator.getString("NewGameDialog"));
		// this.setUndecorated(true);
		this.setResizable(false);

		// Prepare the overall layout: three horizontal boxes
		Box vBox = Box.createVerticalBox();
		this.add(vBox);
		Box topBox = Box.createHorizontalBox();
		Box middleBox = Box.createHorizontalBox();
		Box bottomBox = Box.createHorizontalBox();
		vBox.add(topBox);
		vBox.add(Box.createVerticalStrut(10));
		vBox.add(middleBox);
		vBox.add(Box.createVerticalStrut(10));
		vBox.add(bottomBox);

		// The top box contains the controls for the size of the puzzle
		JLabel lblWidth = new JLabel();
		translator.put(Translatable.NewGameWidth, lblWidth);
		JLabel lblHeight = new JLabel();
		translator.put(Translatable.NewGameHeight, lblHeight);
		cmbWidth = new JComboBox();
		translator.put(Translatable.NewGameWidth, cmbWidth);
		cmbHeight = new JComboBox();
		translator.put(Translatable.NewGameHeight, cmbHeight);
		for (Integer i = 6; i < 15; i++) {
			cmbWidth.addItem(i);
			cmbHeight.addItem(i);
		}
		topBox.add(lblWidth);
		topBox.add(cmbWidth);
		topBox.add(Box.createHorizontalStrut(10));
		topBox.add(lblHeight);
		topBox.add(cmbHeight);

		// The middle box contains the other game options
		chkSymmetrical = new JCheckBox();
		translator.put(Translatable.NewGameSymmetrical, chkSymmetrical);
		JLabel lblDifficulty = new JLabel();
		translator.put(Translatable.NewGameDifficulty, lblDifficulty);
		cmbDifficulty = new JComboBox();
		translator.put(Translatable.NewGameDifficulty, cmbDifficulty);
		for (Integer i = 1; i < 6; i++) {
			cmbDifficulty.addItem(i);
		}
		middleBox.add(chkSymmetrical);
		middleBox.add(Box.createHorizontalStrut(10));
		middleBox.add(lblDifficulty);
		middleBox.add(cmbDifficulty);

		// The bottom box contains the game number and the buttons
		JLabel lblGameNumber = new JLabel();
		translator.put(Translatable.NewGameGameNumber, lblGameNumber);
		txtGameNumber = new JTextField();
		translator.put(Translatable.NewGameGameNumber, txtGameNumber);
		JButton btnStart = new JButton();
		translator.put(Translatable.NewGameStart, btnStart);
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				thisDialog.setVisible(false);
				mainProgram.gameWidth = (Integer) cmbWidth.getSelectedItem();
				mainProgram.gameHeight = (Integer) cmbHeight.getSelectedItem();
				mainProgram.gameDifficulty = (Integer) cmbDifficulty.getSelectedItem();

				try {
					mainProgram.gameNumber = Integer.parseInt(txtGameNumber.getText());
				} catch (NumberFormatException e) {
					mainProgram.gameNumber = -1;
				}

				mainProgram.gameSymmetrical = chkSymmetrical.isSelected();
				mainProgram.startGame = true;
			}
		});
		bottomBox.add(lblGameNumber);
		bottomBox.add(txtGameNumber);
		bottomBox.add(Box.createHorizontalStrut(10));
		bottomBox.add(btnStart);

		translator.setGuiTexts(Translatable.NewDialog);

		// initialization
		mainProgram.startGame = false; // set to true when start-button is pressed
		cmbWidth.setSelectedItem(mainProgram.gameWidth);
		cmbHeight.setSelectedItem(mainProgram.gameHeight);
		cmbDifficulty.setSelectedItem(mainProgram.gameDifficulty);
		chkSymmetrical.setSelected(mainProgram.gameSymmetrical);
		txtGameNumber.setText("");

		this.pack();
	}
}
