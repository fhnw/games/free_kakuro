/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Locale;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import ch.kri.freekakuro.data_classes.CellCell;
import ch.kri.freekakuro.data_classes.Game;
import ch.kri.freekakuro.data_classes.CellSum;
import ch.kri.freekakuro.display_classes.DisplayCell;
import ch.kri.freekakuro.display_classes.DisplayCellCell;
import ch.kri.freekakuro.display_classes.DisplayCellEmpty;
import ch.kri.freekakuro.display_classes.DisplayCellSum;
import ch.kri.freekakuro.translation.Translatable;
import ch.kri.freekakuro.translation.Translate;

public class FreeKakuroGui extends JPanel {
	// Reference to main program
	FreeKakuro mainProgram;
	Object parentFrame; // may be a JFrame or a JApplet
	Translate translator;

	// GUI elements
	private DisplayCell[][] displayCells;
	JPanel puzzleBoard;

	public FreeKakuroGui(FreeKakuro mainProgram, Object parentFrame) {
		super();
		this.mainProgram = mainProgram;
		this.parentFrame = parentFrame;
		this.translator = new Translate("ch.kri.freekakuro.translation.FreeKakuro");

		JMenuBar menuBar = createMenus();
		if ((JApplet.class.isInstance(parentFrame))) {
			((JApplet) parentFrame).setJMenuBar(menuBar);
		} else {
			((JFrame) parentFrame).setJMenuBar(menuBar);
		}

		this.setBackground(FreeKakuro.BLUE_CLEAR);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// The upper controls
		Box upperControls = Box.createHorizontalBox();
		upperControls.add(Box.createHorizontalGlue());

		JButton btnStart = new JButton("Start");
		btnStart.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				startGame();
			}
		});
		upperControls.add(btnStart);

		JButton btnPossVal = new JButton("Show PossVal");
		btnPossVal.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				showPossVal();
			}
		});
		upperControls.add(btnPossVal);

		JButton btnNormal = new JButton("Hide PossVal");
		btnNormal.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				showUserText();
			}
		});
		upperControls.add(btnNormal);

		JButton btnCommit = new JButton("Commit Unique");
		btnCommit.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				commitUnique();
			}
		});
		upperControls.add(btnCommit);

		upperControls.add(Box.createHorizontalGlue());
		this.add(upperControls);
		this.add(Box.createVerticalGlue());

		// The Kakuro puzzle itself - surrounded by flexible glue
		displayCells = new DisplayCell[mainProgram.gameWidth][mainProgram.gameHeight];
		puzzleBoard = new JPanel();
		puzzleBoard.setBorder(new LineBorder(Color.BLACK, 3));
		puzzleBoard.setBackground(Color.BLACK);
		int board_width = 4 + mainProgram.gameWidth * 40;
		int board_height = 4 + mainProgram.gameHeight * 40;
		Dimension dim = new Dimension(board_width, board_height);
		puzzleBoard.setSize(dim);
		puzzleBoard.setMinimumSize(dim);
		puzzleBoard.setMaximumSize(dim);
		puzzleBoard.setPreferredSize(dim);
		Box puzzleBox = Box.createHorizontalBox();
		puzzleBox.add(Box.createHorizontalGlue());
		puzzleBox.add(puzzleBoard);
		puzzleBox.add(Box.createHorizontalGlue());
		this.add(puzzleBox);
		this.add(Box.createVerticalGlue());

		// fill with empty cells - we will put in the real ones later
		for (int i = 0; i < mainProgram.gameWidth; i++) {
			for (int j = 0; j < mainProgram.gameHeight; j++) {
				displayCells[i][j] = new DisplayCellEmpty(i, j);
			}
		}
		fillGrid();

		// The lower controls
		Box lowerControls = Box.createHorizontalBox();
		JLabel x = new JLabel("Lower controls");
		lowerControls.add(x);
		this.add(lowerControls);

		translator.setGuiTexts(Translatable.MainWindow);
	}

	private void fillGrid() {
		puzzleBoard.removeAll();
		int width = displayCells.length;
		int height = displayCells[0].length;
		puzzleBoard.setLayout(new GridLayout(height, width, 2, 2));
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int index = y * width + x;
				puzzleBoard.add(displayCells[x][y], index);
			}
		}
		puzzleBoard.validate();
	}

	private void startGame() {
		GameCreator gc = new GameCreator(mainProgram);
		Game game = gc.createGame();
		mainProgram.game = game;
		mainProgram.solver = new GameSolver(game);
		for (CellSum s : game.sums) {
			int x = s.getX();
			int y = s.getY();

			if (displayCells[x][y] instanceof DisplayCellEmpty) {
				displayCells[x][y] = new DisplayCellSum(x, y, s);
			} else {
				DisplayCellSum dcs = (DisplayCellSum) displayCells[x][y];
				dcs.addSum(s);
			}
		}
		for (CellCell c : game.cells) {
			int x = c.getX();
			int y = c.getY();
			displayCells[x][y] = new DisplayCellCell(x, y, c);
		}
		fillGrid();
	}

	private void showPossVal() {
		Game game = mainProgram.game;
		for (int x = 0; x < game.width; x++) {
			for (int y = 0; y < game.height; y++) {
				if (displayCells[x][y] instanceof DisplayCellCell) {
					DisplayCellCell dcc = (DisplayCellCell) displayCells[x][y];
					dcc.showPossVals();
				}
			}
		}
	}

	private void showUserText() {
		Game game = mainProgram.game;
		for (int x = 0; x < game.width; x++) {
			for (int y = 0; y < game.height; y++) {
				if (displayCells[x][y] instanceof DisplayCellCell) {
					DisplayCellCell dcc = (DisplayCellCell) displayCells[x][y];
					dcc.showUserText();
				}
			}
		}
	}

	private void commitUnique() {
		if (!mainProgram.solver.stepSolver()) {
			System.out.println("No progress!");
		}

		Game game = mainProgram.game;
		for (int x = 0; x < game.width; x++) {
			for (int y = 0; y < game.height; y++) {
				if (displayCells[x][y] instanceof DisplayCellCell) {
					DisplayCellCell dcc = (DisplayCellCell) displayCells[x][y];
					CellCell cc = dcc.getGameCell();
					Integer value = cc.getValue();
					if (value != null && dcc.getText().equals("")) {
						dcc.setText(value.toString());
					}
				}
			}
		}
	}

	private JMenuBar createMenus() {
		JMenuBar menuBar = new JMenuBar();
		JMenu currentMenu;
		JMenuItem currentMenuItem;

		// Create the file menu
		currentMenu = new JMenu();
		translator.put(Translatable.MenuFile, currentMenu);
		menuBar.add(currentMenu);

		currentMenuItem = new JMenuItem();
		currentMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				NewGameDialog.showDialog(mainProgram);
				if (mainProgram.startGame) {
					startGame();
				}
			}
		});
		translator.put(Translatable.MenuFileNew, currentMenuItem);
		currentMenu.add(currentMenuItem);

		currentMenuItem = new JMenuItem();
		currentMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(null, translator.getString("NotImplemented"), translator
						.getString("NotImplemented"), JOptionPane.PLAIN_MESSAGE);
			}
		});
		translator.put(Translatable.MenuFilePrint, currentMenuItem);
		currentMenu.add(currentMenuItem);

		currentMenuItem = new JMenuItem();
		currentMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});
		translator.put(Translatable.MenuFileExit, currentMenuItem);
		currentMenu.add(currentMenuItem);

		// Create the language menu
		currentMenu = new JMenu();
		translator.put(Translatable.MenuLanguage, currentMenu);
		menuBar.add(currentMenu);

		String[] localeNames = Translate.getAvailableLocaleNames();
		Locale[] localeItems = Translate.getAvailableLocales();
		for (int i = 0; i < localeItems.length; i++) {
			final int languageNumber = i; // final constant for the ActionListener
			currentMenuItem = new JMenuItem(localeNames[i]);
			currentMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					// Change the language for all items
					Locale newLocale = Translate.getAvailableLocales()[languageNumber];
					Locale.setDefault(newLocale);
					JComponent.setDefaultLocale(newLocale);
					JFileChooser.setDefaultLocale(newLocale);
					translator.setLocale(newLocale);
					translator.setGuiTexts(Translatable.MainWindow);
					// TODO pack(); // re-pack because lengths of menu-strings have changed
				}
			});
			currentMenu.add(currentMenuItem);
		}
		// Create the help menu
		currentMenu = new JMenu();
		translator.put(Translatable.MenuHelp, currentMenu);
		menuBar.add(currentMenu);

		currentMenuItem = new JMenuItem();
		currentMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(null, translator.getString("NotImplemented"), translator
						.getString("NotImplemented"), JOptionPane.PLAIN_MESSAGE);
			}
		});
		translator.put(Translatable.MenuHelpHelp, currentMenuItem);
		currentMenu.add(currentMenuItem);

		currentMenuItem = new JMenuItem();
		currentMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				StringBuffer msg = new StringBuffer();
				msg.append(translator.getString("HelpAboutText1"));
				msg.append(mainProgram.majorVersion + "." + mainProgram.minorVersion);
				msg.append(translator.getString("HelpAboutText2"));
				JOptionPane.showMessageDialog(null, msg.toString(), translator
						.getString("MenuHelpAbout.tooltip"), JOptionPane.PLAIN_MESSAGE);
			}
		});
		translator.put(Translatable.MenuHelpAbout, currentMenuItem);
		currentMenu.add(currentMenuItem);

		return menuBar;
	}


	@Override
	/**
	 * We add font antialiasing to the standard paint method
	 */
	public void paint(Graphics g) {
		if (g instanceof Graphics2D) {
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		}
		super.paint(g);
	}
}
