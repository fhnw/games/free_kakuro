/**
 * Copyright 2008 Brad Richards (http://richards.kri.ch/)
 * 
 * This file is part of FreeKakuro.
 * 
 * FreeKakuro is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * FreeKakuro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with FreeKakuro; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */
package ch.kri.freekakuro;

import java.util.ArrayList;
import java.util.Iterator;

import ch.kri.freekakuro.data_classes.CellSum;
import ch.kri.freekakuro.data_classes.Game;
import ch.kri.freekakuro.data_classes.CellCell;
import ch.kri.freekakuro.data_classes.PossibleValues;

/**
 * This class solves the Kakuro puzzle represented in the game object. The solver proceeds in steps -
 * sometimes in large steps, but these steps can be controlled with the method stepSolver().
 * 
 * The solver implements a series of strategies, from simple to complex. The simplest strategy is
 * used until it produces no further results. Then the next strategy is tried, and the next and the
 * next - until some step can be made. After any step made by a more complex strategy, the solver
 * reverts again to the simplest. The goal of this process is to track how often the more complex
 * strategies must be used, which gives us a rough estimate of the puzzle's complexity.
 * 
 * @author Brad Richards
 * 
 */
public class GameSolver {
	private Game game; // our reference to the game to be solved

	public GameSolver(Game game) {
		this.game = game;
	}

	/**
	 * This method attempts to perform one step in the solution of the puzzle. If it is able to
	 * perform a step (determine the value of one or more cells), it returns true. Otherwise it
	 * returns false.
	 * 
	 * The method tries the available strategies, beginning with the simplest and proceeding to the
	 * most complex.
	 * 
	 * @return true, if the value of one or more cells could be determined.
	 */
	public boolean stepSolver() {
		boolean returnValue;
		returnValue = strategy0();
		if (!returnValue) returnValue = strategy1();
		if (!returnValue) returnValue = strategy2();
		if (!returnValue) returnValue = strategy3();
		if (!returnValue) returnValue = strategy4();
		if (!returnValue) returnValue = strategy5();
		return returnValue;
	}

	/**
	 * This method is called by strategy methods that restrict possible values, to see if they have
	 * successfully caused one or more cells to have unique values.
	 * 
	 * When we inform sums of committed values, they recalculate the possible values in their cells.
	 * To avoid undesirable interactions, we first collect all cells and their unique values, and only
	 * afterwards do we commit the values.
	 * 
	 * @return true, if the value of one or more cells could be determined.
	 */
	private boolean commitUniqueValues() {
		boolean returnValue;
		ArrayList<CellValuePair> cells = new ArrayList<CellValuePair>();

		// Collect list of all cells with a single possible value
		for (CellCell c : game.cells) {
			if (c.getValue() == null & c.isUnique()) {
				cells.add(new CellValuePair(c, c.getUniqueValue()));
			}
		}
		returnValue = !cells.isEmpty();

		// For each cell, set its value
		for (CellValuePair cvp : cells) {
			cvp.cell.setValue(cvp.value);
			ArrayList<CellSum> sums = cvp.cell.getSums();
			for (CellSum s : sums) {
				s.valueCommitted(cvp.value);
			}
		}
		if (returnValue) System.out.print("0");
		return returnValue;
	}

	/**
	 * Strategy 0 comes "for free" with the implementation of sums and cells. When the sums and cells
	 * are created, the possible values for each cell are restricted to those available in the two
	 * crossing sums. This strategy does nothing more than to set the values of those cells having
	 * only a single possible value.
	 * 
	 * @return true, if the value of one or more cells could be determined.
	 */
	private boolean strategy0() {
		return commitUniqueValues();
	}

	/**
	 * Strategy 1 affects only sums of length 2. We restrict the possible values in each cell based on
	 * the possible values in the other. For example, if we have a sum of 6, theoretically the
	 * possible values for both cells are 1,2,4,5. But if one cell is otherwise restricted to the
	 * values 1,2 then the other cell can be restricted to 4,5.
	 * 
	 * @return true, if the value of one or more cells could be determined.
	 */
	private boolean strategy1() {
		boolean valueEliminated = false;
		ArrayList<CellCell> cells;
		PossibleValues possVals0, possVals1;
		for (CellSum s : game.sums) {
			if (s.getSize() == 2) {
				cells = s.getCells();
				Integer sum = s.getSum();
				possVals0 = cells.get(0).getPossibleValues();
				possVals1 = cells.get(1).getPossibleValues();
				for (Iterator<Integer> i = possVals0.iterator(); i.hasNext();) {
					Integer possVal = i.next();
					Integer requiredPossVal = sum - possVal;
					if (!possVals1.contains(requiredPossVal)) {
						valueEliminated = true;
						i.remove();
					}
				}
				for (Iterator<Integer> i = possVals1.iterator(); i.hasNext();) {
					Integer possVal = i.next();
					Integer requiredPossVal = sum - possVal;
					if (!possVals0.contains(requiredPossVal)) {
						valueEliminated = true;
						i.remove();
					}
				}
			}
		}
		if (valueEliminated) System.out.print("1");
		return valueEliminated;
	}

	/**
	 * Strategy 2 looks for sums where a particular digit must appear (i.e., is present in all
	 * patterns), but is only possible in a single cell. We stop after finding a single such cell.
	 * Note that we do not commit the value ourselves, but let this happen with normal processing
	 * (commitUniqueValues);
	 * 
	 * @return true, if the value of one or more cells could be determined.
	 */
	private boolean strategy2() {
		boolean valueCommitted = false;
		for (CellSum s : game.sums) {
			// Determine which possible values may be of interest
			PossibleValues requiredValues = PossibleValues.intersection(s.getPatterns());

			// only proceed if there is at least one such value
			if (requiredValues.size() >= 1) {
				// Create a list of all cells which do not have committed values
				ArrayList<CellCell> cells = new ArrayList<CellCell>();
				for (CellCell c : s.getCells()) {
					if (c.getValue() == null) {
						cells.add(c);
					}
				}

				// only proceed if there are such cells
				if (cells.size() >= 1) {
					for (Integer value : requiredValues) {
						int numOccurrences = 0;
						CellCell cellOccurring = null;
						for (CellCell c : cells) {
							if (c.getPossibleValues().contains(value)) {
								numOccurrences++;
								cellOccurring = c;
							}
						}
						if (numOccurrences == 1) {
							// Remove all other possible values, making this one unique
							PossibleValues pvs = cellOccurring.getPossibleValues();
							for (Iterator<Integer> i = pvs.iterator(); i.hasNext();) {
								if (i.next() != value) i.remove();
							}
							valueCommitted = true;

							// stop as soon as a single value has been found
							break;
						}
					}
				}
			}

			// stop as soon as a single value has been found
			if (valueCommitted) break;
		}
		if (valueCommitted) System.out.print("2");
		return valueCommitted;
	}

	/**
	 * Strategy 3 elimiates possible values from a cell, if they lead to impossible combinations for
	 * other cells in the sum. Example: suppose we have the following situation:
	 * 
	 * 4 24 5 15 ? ? ? ? ? ? -- ? --
	 * 
	 * The first cell (first ?) has the possible values of 1 and 3. If, however, we were to choose the
	 * 1, then the remaining two cells represent a 2-cell sum of 14. There is no pattern for 14 in 2
	 * cells that will have a non-empty intersection with 5 in 2 cells - hence, we have an
	 * impossibility. The first cell cannot be a 1.
	 * 
	 * To test for this situation, we construct the patterns for a reduced sum, eliminated all cells
	 * with committed values, plus the hypothetical value of the current sum. We test the resulting
	 * possible values against the possible values in each of the remaining cells.
	 * 
	 * Assuming we find a sum where a value can be eliminated, we eliminate all impossible values for
	 * this sum, and then stop.
	 * 
	 * @return true, if at least one possible value could be eliminated
	 */
	private boolean strategy3() {
		boolean valueEliminated = false;
		for (CellSum s : game.sums) {
			// Look for a sum with at least 2 uncommitted cells
			int openCells = 0;
			for (CellCell c : s.getCells()) {
				if (c.getValue() == null) openCells++;
			}
			if (openCells >= 2) {
				// Create a list of the open cells; also determine the openSum
				int openSum = s.getSum();
				ArrayList<CellCell> cells = new ArrayList<CellCell>();
				for (CellCell c : s.getCells()) {
					if (c.getValue() == null) {
						cells.add(c);
					} else {
						openSum -= c.getValue();
					}
				}

				// Check each cell, and within each cell each possible value
				int reducedSize = cells.size() - 1;
				for (CellCell c : cells) {
					for (Iterator<Integer> i = c.getPossibleValues().iterator(); i.hasNext();) {
						Integer possVal = i.next();
						int patternKey = PossibleValues.getPatternKey(reducedSize, openSum - possVal);
						if (!game.patterns.containsKey(patternKey)) {
							// No such pattern exists - eliminate the possible value
							i.remove();
							valueEliminated = true;
						} else {
							// There are such patterns - get them
							ArrayList<PossibleValues> patterns = game.patterns.get(patternKey);
							PossibleValues allPossibleValues = PossibleValues.union(patterns);
							// Check the patterns to see if there is a non-empty intersection with all other cells
							for (CellCell cCheck : cells) {
								if (cCheck != c) {
									PossibleValues intersection = PossibleValues.intersection(allPossibleValues,
											cCheck.getPossibleValues());
									if (intersection.size() == 0) {
										i.remove();
										valueEliminated = true;
										break;
									}
								}
							}
						}
					}
				}
			}

			// stop after finding one sum where we could eliminate possible values
			if (valueEliminated) break;
		}
		if (valueEliminated) System.out.print("3");
		return valueEliminated;
	}

	/**
	 * Strategy 4 is a Sudoku strategy: we look for a pair of cells in a sum that contain the same two
	 * possible values. Because these two cells can only have these two values, these two values must
	 * be present in the cells - and can be eliminated from any other cells in the sum.
	 * 
	 * This method can only apply to sums with at least three uncommitted cells.
	 * 
	 * @return true, if the value of one or more cells could be determined.
	 */
	private boolean strategy4() {
		boolean valueEliminated = false;
		for (CellSum s : game.sums) {
			// Look for a sum with at least 3 uncommitted cells
			int openCells = 0;
			for (CellCell c : s.getCells()) {
				if (c.getValue() == null) openCells++;
			}
			if (openCells >= 3) {
				// Create a list of the open cells
				int openSum = s.getSum();
				ArrayList<CellCell> cells = new ArrayList<CellCell>();
				for (CellCell c : s.getCells()) {
					if (c.getValue() == null) {
						cells.add(c);
					} else {
						openSum -= c.getValue();
					}
				}

				// Look for a first cell with exactly two possible values
				for (int i = 0; i < cells.size(); i++) {
					PossibleValues pv_i = cells.get(i).getPossibleValues();
					if (pv_i.size() == 2) {
						// Look for a second cell with exactly two possible values
						for (int j = i + 1; j < cells.size(); j++) {
							PossibleValues pv_j = cells.get(j).getPossibleValues();
							if (pv_j.size() == 2) {
								// Ensure that the possible-values are identical
								if (PossibleValues.intersection(pv_i, pv_j).size() == 2) {
									// In addition to the given pair of possible values,
									// the remaining cells form a reduced sum: the value
									// of the pair of values can be deducted from the total value for
									// the open cells. Get the set of patterns for this reduced sum,
									// and remove any possible values that are not present in the pattern.
									int sumWithoutPair = openSum - pv_i.sum();
									int openSize = cells.size() - 2;
									PossibleValues pv_reduced = PossibleValues.union(game.patterns
											.get(PossibleValues.getPatternKey(openSize, sumWithoutPair)));

									// See if we can remove any possible-values from other cells
									for (int k = 0; k < cells.size(); k++) {

										// For all cells other than the pair
										if (i != k & j != k) {
											// Get the possible values for this cell, and note how many there are
											PossibleValues pv_k = cells.get(k).getPossibleValues();
											int startingSize = pv_k.size();

											// try removing the values from the pair
											pv_k.removeAll(pv_i);

											// try removing values not in the reduced pattern
											pv_k.retainAll(pv_reduced);

											// see if we removed anything
											if (startingSize > pv_k.size()) valueEliminated = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (valueEliminated) System.out.print("4");
		return valueEliminated;
	}

	/**
	 * Strategy 5 is a Sudoku strategy: we look for a triplet of cells in a sum that contain the same
	 * three possible values. Because these three cells can only have these three values, these three
	 * values must be present in the cells - and can be eliminated from any other cells in the sum.
	 * 
	 * Note: it is acceptable for each cell to contain only two of the three values, e.g., for the
	 * triplet 789 one cell may contain 78, another 79 and a third 89.
	 * 
	 * This method can only apply to sums with at least four uncommitted cells.
	 * 
	 * @return true, if the value of one or more cells could be determined.
	 */
	private boolean strategy5() {
		boolean valueEliminated = false;
		// Can we simultaneously look for quadruplets?
		
		// for (CellSum s : game.sums) {
		// // Look for a sum with at least 4 uncommitted cells
		// int openCells = 0;
		// for (CellCell c : s.getCells()) {
		// if (c.getValue() == null) openCells++;
		// }
		// if (openCells >= 4) {
		// System.out.println("Strategy 5 not implemented!");
		// }
		//
		// // stop after finding one sum where we could eliminate possible values
		// if (valueEliminated) break;
		// }
		return valueEliminated;
	}

	private class CellValuePair {
		CellCell cell;
		int value;

		CellValuePair(CellCell cell, int value) {
			this.cell = cell;
			this.value = value;
		}
	}
}
